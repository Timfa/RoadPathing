﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * GridSnap
 * Created by: Tim Falken
 **/

[ExecuteInEditMode]
public class GridSnap : MonoBehaviour 
{
	[SerializeField] private float gridSize = 2;

	void Start () 
	{
		Vector3 pos = transform.position;

		pos.x = Mathf.Round (pos.x / gridSize) * gridSize;
		pos.z = Mathf.Round (pos.z / gridSize) * gridSize;

		transform.position = pos;
	}

	#if UNITY_EDITOR

	void LateUpdate()
	{
		if (!UnityEditor.EditorApplication.isPlaying)
			Start ();
	}

	#endif
}

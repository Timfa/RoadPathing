﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * NavigatorTask
 * Created by: Tim Falken
 **/

public class NavigatorTask 
{
	public ExitPoint Pickup;
	public bool PickupDone;

	public ExitPoint Dropoff;
	public bool DropoffDone;
}

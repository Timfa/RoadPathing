﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * TweenUpdater
 * Created by: Tim Falken
 * 
 * Place in a scene to automagically update all tweens.
 **/

public class TweenUpdater : MonoBehaviour 
{
	void Update () 
	{
		Tween.UpdateTweens (Time.deltaTime);
	}
}

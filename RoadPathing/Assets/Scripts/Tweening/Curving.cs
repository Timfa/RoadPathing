﻿using System;
using UnityEngine;

/**
 * Curving
 *Created by: Tim Falken
 * 
 *Allows you to do Bezier Curves with floats and Vector3's using an arbitrary amount of points.
 **/

public class Curving
{
	public static float Lerp(float[] points, float t)
	{
		if (points.Length == 0)
		{
			return 0;
		}
		else if (points.Length == 1)
		{
			return points [0];
		}
		else if (points.Length == 2)
		{
			return Mathf.Lerp (points [0], points [1], t);
		} 
		else
		{
			float[] newPoints = new float[points.Length - 1];

			for (int i = 0; i < newPoints.Length; i ++)
			{
				newPoints [i] = Mathf.Lerp (points [i], points [i + 1], t);
			}

			return Lerp (newPoints, t);
		}
	}

	public static Vector3 Lerp(Vector3[] points, float t, bool debugDraw = false)
	{
		Color color = Color.cyan;

		#if UNITY_EDITOR
		if(UnityEditor.EditorApplication.isPlaying)
			color = Color.blue;
		#endif

		if (points.Length == 0)
		{
			return Vector3.zero;
		}
		else if (points.Length == 1)
		{
			return points [0];
		}
		else if (points.Length == 2)
		{
			if (debugDraw)
			{
				Debug.DrawLine (points [0], points [1], color);
			}

			return Vector3.Lerp (points [0], points [1], t);
		} 
		else
		{
			Vector3[] newPoints = new Vector3[points.Length - 1];

			for (int i = 0; i < newPoints.Length; i ++)
			{
				if (debugDraw)
				{
					

					Debug.DrawLine (points [i], points [i + 1], color);
				}

				newPoints [i] = Vector3.Lerp (points [i], points [i + 1], t);	
			}

			return Lerp (newPoints, t, debugDraw);
		}
	}

	public static ICurvable Lerp(ICurvable[] points, float t)
	{
		if (points.Length == 0)
		{
			throw new ArgumentNullException ("Given points array is empty!");
		}
		else if (points.Length == 1)
		{
			return points [0];
		}
		else if (points.Length == 2)
		{
			return points [0].add(points [1].subtract(points[0]).multiply(t));
		} 
		else
		{
			ICurvable[] newPoints = new ICurvable[points.Length - 1];

			for (int i = 0; i < newPoints.Length; i ++)
			{
				newPoints [i] = points [i].add(points [i + 1].subtract(points[i]).multiply(t));
			}

			return Lerp (newPoints, t);
		}
	}

	public interface ICurvable
	{
		ICurvable add(ICurvable other);
		ICurvable subtract(ICurvable other);
		ICurvable multiply(float factor);
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * PathingNode
 * Created by: Tim Falken
 **/


public class PathingEntranceNode
{
	public int id;

	public PathingNode.ExitData associatedExit;
	public PathingNode[] nodes;
}

public class PathingNode 
{
	public int pathId;

	public int entranceId;
	public ExitData exitData;

	public class ExitData
	{
		public int id;

		public int connectedEntranceId = -1;

		public bool parkingSpot;
		public bool parkingSpotTaken;

		public PathingEntranceNode connectedNode;
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Item
 * Created by: Tim Falken
 **/

public class Item : MonoBehaviour 
{
	public ID Id;

	public enum ID
	{
		Red,
		Green,
		Blue,
		Product
	}
}

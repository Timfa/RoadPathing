﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * EntrancePoint
 * Created by: Tim Falken
 **/

[ExecuteInEditMode]
public class EntrancePoint : MonoBehaviour 
{
	public Path[] Paths;

	private static int idCounter = 0;

	public int Id;

	public PathingEntranceNode pathingNode
	{
		get
		{
			PathingEntranceNode node = new PathingEntranceNode();

			PathingNode[] nodes = new PathingNode[Paths.Length];

			for (int i = 0; i < Paths.Length; i++)
			{
				nodes[i] = new PathingNode ();

				nodes[i].pathId = Paths[i].Id;

				nodes [i].entranceId = Id;

				nodes[i].exitData = new PathingNode.ExitData ();

				nodes[i].exitData.id = Paths [i].Exit.Id;

				nodes[i].exitData.parkingSpot = Paths [i].Exit.IsParkingSpot;
				nodes[i].exitData.parkingSpotTaken = Paths [i].Exit.ParkSpotTaken;

				if(Paths[i].Exit.ConnectedPoint != null)
					nodes [i].exitData.connectedEntranceId = Paths [i].Exit.ConnectedPoint.Id;
			}

			node.id = Id;
			node.nodes = nodes;

			return node;
		}
	}

	public static PathingEntranceNode[] GlobalNodes
	{
		get
		{
			EntrancePoint[] points = GameObject.FindObjectsOfType<EntrancePoint> ();
			PathingEntranceNode[] nodes = new PathingEntranceNode[points.Length];

			for (int i = 0; i < points.Length; i++)
				nodes [i] = points [i].pathingNode;

			for (int i = 0; i < nodes.Length; i++)
			{
				for (int i2 = 0; i2 < nodes.Length; i2++)
				{
					foreach (PathingNode path in nodes[i2].nodes)
					{
						if (nodes [i].id == path.exitData.connectedEntranceId)
						{
							path.exitData.connectedNode = nodes [i];
							nodes [i].associatedExit = path.exitData;
						}
					}
				}
			}

			return nodes;
		}
	}

	public EntrancePoint[] Connections
	{
		get
		{
			List<EntrancePoint> points = new List<EntrancePoint> ();

			for (int i = 0; i < Paths.Length; i++)
			{
				points.Add (Paths [i].Exit.ConnectedPoint);
			}

			return points.ToArray ();
		}
	}

	public Path GetPathForConnection(EntrancePoint connection)
	{
		for (int i = 0; i < Paths.Length; i++)
		{
			if (Paths [i].Exit.ConnectedPoint == connection)
			{
				return Paths [i];
			}
		}

		return null;
	}

	public Vector3 GetPointOnPath(Path path, float t, bool draw = true)
	{
		Vector3[] points = new Vector3[path.MidPoints.Length + 2];

		points [0] = transform.position;

		points [points.Length - 1] = path.Exit.ConnectedPoint == null? path.Exit.transform.position : path.Exit.ConnectedPoint.transform.position;

		for (int i = 1; i < points.Length - 1; i++)
		{
			points [i] = transform.TransformPoint(path.MidPoints [i - 1]);
		}

		return Curving.Lerp (points, t, draw);
	}

	void Awake()
	{
		for (int i = 0; i < Paths.Length; i++)
		{
			Paths [i].SetOwner (this);
		}

		#if UNITY_EDITOR
		if(!UnityEditor.EditorApplication.isPlaying)
			return;
		#endif

		for (int i = 0; i < Paths.Length; i++)
		{
			Paths [i].Id = idCounter++;
		}

		Id = idCounter++;

		GetComponent<Renderer> ().enabled = false;
	}

	#if UNITY_EDITOR

	void Update()
	{
		if (!UnityEditor.EditorApplication.isPlaying)
		{
			for (int i = 0; i < Paths.Length; i++)
			{
				Paths [i].SetOwner (this);
			}

			for (int i = 0; i < Paths.Length; i++)
			{
				GetPointOnPath(Paths[i], 0.5f);

				int amt = (Paths[i].MidPoints.Length + 2) * 50;

				for (int a = 0; a < amt; a++)
				{
					float t1 = (float)a / amt;
					float t2 = (float)(a + 1) / amt;

					Debug.DrawLine (GetPointOnPath(Paths[i], t1, false), GetPointOnPath(Paths[i], t2, false), Color.red);
				}
			}
		}
	}

	#endif

	[System.Serializable]
	public class Path
	{
		public int Id;
		public Vector3[] MidPoints;
		public ExitPoint Exit;

		public EntrancePoint Owner{ get; private set; }

		public void SetOwner(EntrancePoint newOwner)
		{
			Owner = newOwner;
		}

		public Vector3 GetPointOnPath(Path path, float t, bool draw = true)
		{
			return Owner.GetPointOnPath (path, t, draw);
		}
	}
}

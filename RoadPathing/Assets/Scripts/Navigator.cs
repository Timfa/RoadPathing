﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;

/**
 * Navigator
 * Created by: Tim Falken
 **/

[RequireComponent(typeof(PathFinder))]
public class Navigator : MonoBehaviour 
{
	public EntrancePoint.Path currentPoint
	{
		get
		{
			return currentPath.Count == 0? null : currentPath [0];
		}
	}

	private PathFinder pathFinder;
	private List<EntrancePoint.Path> currentPath = new List<EntrancePoint.Path>();

	private Tween tween;
	private Thread searcherThread;

	private bool finding = false;
	private bool navigating = false;

	private ExitPoint currentParkNode = null;

	public NavigatorTask CurrentTask { get; private set; } 

	public bool Parked
	{
		get
		{
			return CurrentTask == null && currentPath.Count == 0;
		}
	}

	public Item Item
	{
		get
		{
			return GetComponentInChildren<Item> ();
		}

		set
		{
			value.transform.parent = itemSlot;
			value.transform.localPosition = Vector3.zero;
			value.transform.localRotation = Quaternion.identity;
		}
	}

	[SerializeField] private Transform itemSlot;

	[SerializeField] private Transform[] headlightTransforms;

	void Start () 
	{
		pathFinder = GetComponent<PathFinder> ();

		pathFinder.navigator = this;

		FindParkingSpot ();

		transform.position = currentParkNode.transform.position;
	}

	void Update()
	{
		if (currentPath.Count > 0)
		{
			Debug.DrawLine (transform.position, currentPath [currentPath.Count - 1].Exit.transform.position, Color.black);

			for (int i = 0; i < currentPath.Count - 1; i++)
			{
				Debug.DrawLine (currentPath [i].Exit.transform.position, currentPath [i + 1].Exit.transform.position, Color.black);
			}

			if (!navigating)
			{
				NavigatePath ();
			}
		} 
		else if (!finding)
		{
			if (currentParkNode != null)
			{
				if (Vector3.Distance (transform.position, currentParkNode.transform.position) > 1)
				{
					PathToNextObjective ();
				}
			}

			if (CurrentTask != null)
			{
				if (Vector3.Distance (transform.position, CurrentTask.Dropoff.transform.position) > 1 && Vector3.Distance (transform.position, CurrentTask.Pickup.transform.position) > 1)
				{
					PathToNextObjective ();
				}
			}
		}

		if (tween == null)
			return;

		foreach (Transform light in headlightTransforms)
		{
			Ray ray = new Ray (light.transform.position, (light.transform.forward + (light.transform.right * light.transform.localPosition.x * 3)).normalized);
			tween.paused = Physics.Raycast (ray, 0.6f);

			Debug.DrawLine (light.transform.position, light.transform.position + (light.transform.forward + (light.transform.right * light.transform.localPosition.x * 3)).normalized * 0.6f, tween.paused ? Color.red : Color.green);
		}
	}

	void FindPathTo(EntrancePoint start, ExitPoint exit)
	{
		finding = true;

		pathFinder.FindPathToExitNode (start, exit);
	}

	public void SetPath(EntrancePoint.Path[] path)
	{
		currentPath.AddRange(path);

		Debug.Log(path);

		finding = false;
	}

	private EntrancePoint ClosestEntrance ()
	{
		EntrancePoint[] points = FindObjectsOfType<EntrancePoint> ();

		EntrancePoint result = null;

		float d = Mathf.Infinity;

		foreach (EntrancePoint p in points)
		{
			float cD = Vector3.Distance (transform.position, p.transform.position);

			if (cD < d)
			{
				d = cD;
				result = p;
			}
		}

		return result;
	}

	public void AssignTask(NavigatorTask task)
	{
		CurrentTask = task;

		if (navigating)
		{
			currentPath = new List<EntrancePoint.Path> ();
		} 
		else
		{
			PathToNextObjective ();
		}
	}

	private void PathToNextObjective()
	{
		if (CurrentTask != null)
		{
			if (!CurrentTask.PickupDone)
			{
				if (currentParkNode != null)
				{
					currentParkNode.ParkSpotTaken = false;
					currentParkNode = null;
				}

				FindPathTo (ClosestEntrance (), CurrentTask.Pickup);
			} 
			else if (!CurrentTask.DropoffDone)
			{
				if (currentParkNode != null)
				{
					currentParkNode.ParkSpotTaken = false;
					currentParkNode = null;
				}

				FindPathTo (ClosestEntrance (), CurrentTask.Dropoff);
			} 
			else
			{
				CurrentTask = null;
			}
		}

		if (CurrentTask == null && currentParkNode == null)
		{
			FindParkingSpot ();
			FindPathTo (ClosestEntrance (), currentParkNode);
		}
	}

	private void FindParkingSpot()
	{
		ExitPoint[] points = GameObject.FindObjectsOfType<ExitPoint> ().ToList().FindAll(o =>o.IsParkingSpot && !o.ParkSpotTaken).ToArray();

		float d = Mathf.Infinity;

		ExitPoint exit = null;

		foreach (ExitPoint p in points)
		{
			float cD = Vector3.Distance (p.transform.position, transform.position);

			if (cD < d)
			{
				d = cD;
				exit = p;
			}
		}

		currentParkNode = exit;

		currentParkNode.ParkSpotTaken = true;
	}

	void NavigatePath()
	{
		navigating = true;
		EntrancePoint.Path path = currentPoint;

		if (path == null)
			return;

		tween = new Tween (Easing.Linear);

		tween.onChange = t =>
		{
			if(currentPoint != null)
			{
				transform.LookAt(currentPoint.GetPointOnPath(path, t));
				transform.position = currentPoint.GetPointOnPath(path, t);
			}
		};

		tween.onComplete = () =>
		{
			if(currentPath != null && currentPath.Count > 0)
			{
				currentPath[0].Exit.RegisterPass(this);

				if(CurrentTask != null)
				{
					if(currentPath[0].Exit == CurrentTask.Pickup)
					{
						CurrentTask.PickupDone = true;
					}

					if(currentPath[0].Exit == CurrentTask.Dropoff)
					{
						CurrentTask.DropoffDone = true;
					}
				}
				
				currentPath.RemoveAt(0);

				if(currentPoint != null)
				{
					NavigatePath();
				}
				else
				{
					navigating = false;

					PathToNextObjective();
				}
			}
		};

		tween.Start (0, 1, (float)path.MidPoints.Length * 0.5f + 1f);
	}
}

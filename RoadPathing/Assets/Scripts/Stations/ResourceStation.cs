﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * ResourceStation
 * Created by: Tim Falken
 **/

public class ResourceStation : Station 
{
	[SerializeField] private Item resourcePrefab;

	void Start () 
	{
		ConnectedPoint.OnPassEvent = navigator =>
		{
			if(navigator.CurrentTask != null && navigator.CurrentTask.Pickup == ConnectedPoint)
			{
				navigator.Item = Instantiate(resourcePrefab);
			}
		};
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * ItemDestructor
 * Created by: Tim Falken
 **/

public class ItemDestructor : Station 
{
	void Start () 
	{
		ConnectedPoint.OnPassEvent = navigator =>
		{
			if(navigator.CurrentTask != null && navigator.CurrentTask.Dropoff == ConnectedPoint)
			{
				Destroy(navigator.Item.gameObject);
			}
		};
	}
}

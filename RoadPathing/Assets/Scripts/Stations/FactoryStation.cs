﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/**
 * FactoryStation
 * Created by: Tim Falken
 **/

public class FactoryStation : Station 
{
	[SerializeField] private Item productPrefab;

	[SerializeField] private ResourceStation redStation, blueStation, greenStation;
	[SerializeField] private ItemDestructor destructor;

	private bool hasRed, hasBlue, hasGreen;

	void Start () 
	{
		ConnectedPoint.OnPassEvent = navigator =>
		{
			if(navigator.CurrentTask != null && navigator.CurrentTask.Pickup == ConnectedPoint)
			{
				navigator.Item = Instantiate(productPrefab);
			}

			if(navigator.CurrentTask != null && navigator.CurrentTask.Dropoff == ConnectedPoint)
			{
				Item item = navigator.Item;

				if(item == null)
					return;

				if(item.Id == Item.ID.Red)
				{
					Destroy(navigator.Item.gameObject);
					hasRed = true;
				}

				if(item.Id == Item.ID.Green)
				{
					Destroy(navigator.Item.gameObject);
					hasGreen = true;
				}

				if (item.Id == Item.ID.Blue)
				{
					Destroy(navigator.Item.gameObject);
					hasBlue = true;
				}

				if(hasRed && hasGreen && hasBlue)
				{
					Navigator newNav = GameObject.FindObjectsOfType<Navigator>().FirstOrDefault(o => o.Parked);

					NavigatorTask task = new NavigatorTask();

					task.Pickup = ConnectedPoint;
					task.Dropoff = destructor.ConnectedPoint;

					newNav.AssignTask(task);

					AssignFetchTasks();

					hasRed = false;
					hasGreen = false;
					hasBlue = false;
				}
			}
		};

		AssignFetchTasks ();
	}

	private void AssignFetchTasks()
	{
		Navigator[] navs = GameObject.FindObjectsOfType<Navigator> ();

		//red
		Navigator newNavR = navs.FirstOrDefault(o => o.Parked);

		NavigatorTask taskR = new NavigatorTask();

		taskR.Pickup = redStation.ConnectedPoint;
		taskR.Dropoff = ConnectedPoint;

		newNavR.AssignTask(taskR);

		//green
		Navigator newNavG = navs.FirstOrDefault(o => o.Parked);

		NavigatorTask taskG = new NavigatorTask();

		taskG.Pickup = greenStation.ConnectedPoint;
		taskG.Dropoff = ConnectedPoint;

		newNavG.AssignTask(taskG);

		//blue
		Navigator newNavB = navs.FirstOrDefault(o => o.Parked);

		NavigatorTask taskB = new NavigatorTask();

		taskB.Pickup = blueStation.ConnectedPoint;
		taskB.Dropoff = ConnectedPoint;

		newNavB.AssignTask(taskB);
	}
}

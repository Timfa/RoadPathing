﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * StationConnector
 * Created by: Tim Falken
 **/

public abstract class Station : MonoBehaviour 
{
	public ExitPoint ConnectedPoint;
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * ExitPoint
 * Created by: Tim Falken
 **/

[ExecuteInEditMode]
public class ExitPoint : MonoBehaviour 
{
	public EntrancePoint ConnectedPoint;
	public OnExitPassEvent OnPassEvent;

	public bool IsParkingSpot = false;
	public bool ParkSpotTaken = false;

	public int Id;
	private static int idCounter = 0;

	void Awake()
	{
		#if UNITY_EDITOR
		if(!UnityEditor.EditorApplication.isPlaying)
			return;
		#endif

		Id = idCounter++;

		ParkSpotTaken = false;

		ConnectToClosestPoint ();
		GetComponent<Renderer> ().enabled = false;
	}

	void LateUpdate()
	{
		ConnectToClosestPoint ();

		if(ConnectedPoint != null)
			Debug.DrawLine (transform.position + transform.up * 0.1f, ConnectedPoint.transform.position + ConnectedPoint.transform.up * 0.1f, Color.green);
	}

	public void RegisterPass(Navigator navigator)
	{
		if (OnPassEvent != null)
			OnPassEvent (navigator);
	}

	private void ConnectToClosestPoint()
	{
		ConnectedPoint = null;
		EntrancePoint[] points = FindObjectsOfType<EntrancePoint> ();

		float d = 0.3f;

		foreach (EntrancePoint p in points)
		{
			float cD = Vector3.Distance (transform.position, p.transform.position);

			if (cD < d)
			{
				d = cD;
				ConnectedPoint = p;
			}
		}
	}

	public delegate void OnExitPassEvent(Navigator navigator);
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;

/**
 * PathFinder
 * Created by: Tim Falken
 **/

public class PathFinder : MonoBehaviour 
{
	private bool readyToReturn;
	private PathingNode[] pathToReturn = null;

	private int pathId = 0;

	private static PathingEntranceNode[] _allNodes;
	private static PathingEntranceNode[] allNodes
	{
		get
		{
			if (_allNodes == null)
			{
				_allNodes = EntrancePoint.GlobalNodes;
			}

			return _allNodes;
		}
	}

	private Thread searcherThread;
	public Navigator navigator;

	public void FindPathToExitNode(EntrancePoint start, ExitPoint exit)
	{
		Debug.Log ("Finding a path");

		PathingEntranceNode startNode = allNodes.First(o => o.id == start.Id);
		PathingNode.ExitData exitNode = allNodes.First(o => o.associatedExit.id == exit.Id).associatedExit;

		if (searcherThread != null)
		{
			Debug.Log ("previous search aborted");
			searcherThread.Interrupt();
			searcherThread.Abort ();
		}

		pathId++;

		searcherThread = new Thread (() =>
		{
			int thisId = pathId;
			PathingNode[][] foundPaths = new PathingNode[start.Paths.Length][];

			for (int i = 0; i < start.Paths.Length; i++)
			{
				if(pathId != thisId)
				{
					return;
				}

				PathCrawler crawler = new PathCrawler ();

				PathingNode[] found = crawler.FindPathToExitNode (startNode.nodes.First(o => o.pathId == start.Paths [i].Id), exitNode, new List<PathingNode.ExitData> ());

				foundPaths [i] = found;
			}

			int steps = int.MaxValue;
			PathingNode[] selected = null;

			for (int i = 0; i < foundPaths.Length; i++)
			{
				if (foundPaths [i] != null && foundPaths [i].Length < steps)
				{
					steps = foundPaths [i].Length;
					selected = foundPaths [i];
				}
			}

			Debug.Log (selected == null ? "No path found" : "Path with " + selected.Length + " steps found.");

			if(pathId == thisId)
			{
				pathToReturn = selected;
				readyToReturn = true;
			}
		});

		searcherThread.Priority = System.Threading.ThreadPriority.Lowest;

		Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Highest;

		searcherThread.Start ();
	}

	void Update()
	{
		if (readyToReturn)
		{
			readyToReturn = false;

			searcherThread = null;

			if (pathToReturn == null)
			{
				navigator.SetPath (new EntrancePoint.Path[0]);
			} 
			else
			{
				EntrancePoint.Path[] convertedPath = new EntrancePoint.Path[pathToReturn.Length];

				EntrancePoint[] points = GameObject.FindObjectsOfType<EntrancePoint> ();

				for (int i = 0; i < convertedPath.Length; i++)
				{
					convertedPath [i] = points.First (o => o.Id == pathToReturn [i].entranceId).Paths.First (o => o.Id == pathToReturn [i].pathId);
				}

				navigator.SetPath (convertedPath);
			}

			pathToReturn = null;
		}
	}

	public class PathCrawler
	{
		public PathingNode[] FindPathToExitNode(PathingNode start, PathingNode.ExitData exit, List<PathingNode.ExitData> visited)
		{
			if (start.exitData == exit)
			{
				return new PathingNode[]{ start };
			}

			if(visited.Contains(start.exitData))
			{
				return null;
			}

			visited.Add (start.exitData);

			if (start.exitData == null)
				return null;

			if (start.exitData.connectedNode == null)
				return null;

			if (start.exitData.connectedNode.nodes == null)
				return null;

			if (start.exitData.connectedNode.nodes.Length == 0)
				return null;

			if (start.exitData.parkingSpot)
				return null;

			int length = start.exitData.connectedNode.nodes.Length;

			PathingNode[][] foundPaths = new PathingNode[start.exitData.connectedNode.nodes.Length][];

			for (int i = 0; i < length; i++)
			{
				if (start.exitData.connectedNode.nodes [i] != null)
				{
					PathCrawler crawler = new PathCrawler ();
					PathingNode[] found = crawler.FindPathToExitNode (start.exitData.connectedNode.nodes [i], exit, new List<PathingNode.ExitData> (visited));

					foundPaths[i] = found;
				}
			}

			int steps = int.MaxValue;
			PathingNode[] selected = null;

			for (int i = 0; i < foundPaths.Length; i++)
			{
				if (foundPaths[i] != null && foundPaths [i].Length < steps)
				{
					steps = foundPaths [i].Length;
					selected = foundPaths [i];
				}
			}

			if (selected != null)
			{
				PathingNode[] result = new PathingNode[selected.Length + 1];

				for (int i = 1; i < result.Length; i++)
				{
					result [i] = selected [i - 1];
				}

				result [0] = start;

				return result;
			}

			return null;
		}
	}

	public delegate void OnPathFoundCallback(EntrancePoint.Path[] path);
}